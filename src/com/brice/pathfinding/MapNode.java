package com.brice.pathfinding;

public class MapNode {
	
	// These are examples, and by no means what you HAVE to use for the mask, except for 1.
	public static int TYPE_NPC = 3;
	public static int TYPE_PLAYER = 2;
	public static int TYPE_ALL = 1;
	
	public int cost;
	public byte blockMask;
	
	/**
	 * Sets this node's blocking mask to block blockId.
	 * @param blockID
	 */
	public void block(int blockID) {
		blockMask |= 1 << blockID;
	}
	
	public void unBlock(int blockID) {
		blockMask &= ~(1 << blockID);
	}
	
	/**
	 * @param blockID
	 * @return true if this tile blocks the calling type, or everything.
	 */
	public boolean blocks(int blockID) {	
//		System.out.println((blockMask & (1 << blockID)) != 0 || (blockMask & (1 << TYPE_ALL)) != 0);
		return (blockMask & (1 << blockID)) != 0 || (blockMask & (1 << TYPE_ALL)) != 0;
	}
	
	public boolean isFree() {
		return blockMask == 0;
	}
	
	@Override
	public String toString() {
		return "[Cost: "+cost+"][blockMask: "+blockMask+"]";
	}
}
