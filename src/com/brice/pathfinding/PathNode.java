package com.brice.pathfinding;

public class PathNode {
	
	public int f;
	public int g;
	public int h;
	
	public int x;
	public int y;
	
	public PathNode parent;
	
	public PathNode(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "[X:"+x+" Y:"+y+" F:"+f+" G:"+g+" H:"+h+"]";
	}
}